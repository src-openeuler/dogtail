Name:          dogtail
Version:       0.9.11
Release:       1
Summary:       Tools for GUI testing automatically
License:       GPLv2
URL:           https://gitlab.com/dogtail/dogtail/
Source0:       https://gitlab.com/dogtail/dogtail/-/archive/DOGTAIL_0_9_11/dogtail-DOGTAIL_0_9_11.tar.gz
BuildArch:     noarch

BuildRequires: desktop-file-utils

%description
GUI test tool and automation framework that uses assistive technologies to
communicate with desktop applications.
It uses Accessibility (a11y) technologies.

%package -n python3-dogtail
Summary:       GUI test tool and automation framework - python3 installation
BuildRequires: python3-devel python3-setuptools
Requires:      python3-pyatspi python3-gobject python3-cairo rpm-python3
Requires:      xorg-x11-xinit hicolor-icon-theme

%description -n python3-dogtail
GUI test tool and automation framework that uses assistive technologies to
communicate with desktop applications.

%package_help

%prep
%autosetup -n %{name}-DOGTAIL_0_9_11 -p1
rm -rf %{py3dir}
cp -a . %{py3dir}

%build
cd  %{py3dir}
%{__python3} setup.py build
cd -

%install
cd  %{py3dir}
%{__python3} ./setup.py install -O2 --root=$RPM_BUILD_ROOT --record=%{name}.files
cd -

find examples -type f -exec chmod 0644 \{\} \;
desktop-file-install $RPM_BUILD_ROOT/%{_datadir}/applications/sniff.desktop \
  --dir=$RPM_BUILD_ROOT/%{_datadir}/applications \

%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -n python3-dogtail
%{_bindir}/*
%{python3_sitelib}/dogtail/
%{_datadir}/applications/*
%{_datadir}/dogtail/
%{_datadir}/icons/hicolor/*/apps/%{name}*.*
%{python3_sitelib}/%{name}-%{version}-py%{python3_version}.egg-info
%doc COPYING README NEWS
%doc examples/

%files help
%{_docdir}/dogtail

%changelog
* Thu Aug 17 2023 cky1122 <cky2536184321@163.com> - 0.9.11-1
- Update to upstream version 0.9.11

* Wed Feb 08 2023 chendingjian <chendingjian@kylinsec.com.cn> - 0.9.10-4
- Add python%{python3_version}dist(dogtail) to solve the problem that the package cannot be provided.

* Thu Oct 22 2020 huanghaitao <huanghaitao8@huawei.com> - 0.9.10-3
- Disable python2 module

* Wed Mar 04 2020 yangjian<yangjian79@huawei.com> - 0.9.10-2
- Package init
